<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('front.index');
    // return redirect(route('login'));
});
// route for Auth
Auth::routes();
// verify
Auth::routes(['verify' => true]); 

Route::prefix('user')->group(function(){
    Route::get('/dashboard', 'User\DashboardController@index')->name('dashboard');    
    Route::get('/profile', 'User\ProfileController@index')->name('profile');   
    Route::post('/profile/save', 'User\ProfileController@update')->name('profile.save'); 
    Route::post('/profile/password/save', 'User\ProfileController@updatePassword')->name('profile.password'); 
    Route::post('/profile/withdrawal/address/save', 'User\ProfileController@updateWA')->name('profile.wallet');   
    Route::get('/buy-token', 'User\TransactionController@index')->name('buy-token');   
    Route::get('/transactions', 'User\TransactionController@getTrans')->name('transactions');   
    Route::post('/transactions/new', 'User\TransactionController@create')->name('transaction.new');   
});