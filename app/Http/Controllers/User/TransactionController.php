<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Transaction;
use Auth;

class TransactionController extends Controller
{
    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

   /**
    * Show the application dashboard.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $bonus = 20;
        $ETH = 30;
        $USD = 50;
        $response =[
            'ETH' => $ETH,
            'USD' =>$USD,
            'bonus' => $bonus
        ];
        //    dd(Auth::user()->profile);
        return view('user.pages.buy-token', $response);
    }
   
    public function getTrans(Transaction $transaction)
    {
        $trans = $transaction->where('user_id', Auth::user()->id);
        $response = [
            'transactions' =>$trans
        ];
        //    dd(Auth::user()->profile);
        return view('user.pages.transactions', $response);
    }

    public function create(Request $request){
        // dd($request->all());
        $input = $request->all();

        return response()->json(['success'=>'Got Simple Ajax Request.', 'data' => $input], 200)->withCallback($request->input('callback'));
    }
}
