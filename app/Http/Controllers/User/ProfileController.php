<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Auth;
use App\Models\User;
use App\Models\profile;

class ProfileController extends Controller
{
    /**
    * Create a new controller instance.
    *
    * @return void
    */
   public function __construct()
   {
       $this->middleware(['auth', 'verified']);
   }

   /**
    * Show the application dashboard.
    *
    * @return \Illuminate\Http\Response
    */
   public function index()
   {
    //    dd(Auth::user()->withdrawlAddress->address);
       return view('user.pages.profile');
   }

   public function update(Request $request, profile $profile){
       $user = User::where('id', Auth::user()->id)->first();
       $saved = $profile->saveProfile($request, $user);
       if($saved){
            return back()->with('success', 'Profile updated Successfully');
       }else{
            return back()->with('error', 'An error Occurred');
       }
   }

   public function updatePassword(Request $request)
   {
       $user = User::findOrFail(Auth::user()->id);
       /*
       * Validate all input fields
       */
        $valid = $request->validate([
            'old_password' => 'required|min:8',
            'password' => 'required|confirmed|min:8',
        ]);
        if($valid){
            // dd($request->all());
            if (Hash::check($request->old_password, $user->password)) { 
                $user->fill([
                'password' => Hash::make($request->password)
                ])->save();

                return back()->with('success', 'Password changed Successfully');

            } else {
                return back()->with('error', 'Password does not match');
            }
        }
   
   }

   public function updateWA(Request $request, profile $profile){
       $user = User::where('id', Auth::user()->id)->first();
       $saved = $profile->setWidthrawalAddress($request, $user);
       if($saved){
            return back()->with('success', 'Withdrawal Address updated Successfully');
       }else{
            return back()->with('error', 'An error Occurred, Please try Again');
       }
   }
}
