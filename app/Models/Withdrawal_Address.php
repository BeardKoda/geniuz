<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Withdrawal_Address extends Model
{
    protected $fillable = ['user_id', 'currency', 'address', 'amount', 'update'];
    
    public function user(){
        return $this->belongsTo('App\Models\User');
    }
}
