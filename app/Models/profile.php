<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Wallet;
use App\Models\Withdrawal_Address as WAddress;

class profile extends Model
{
    //
    protected $fillable = ['user_id', 'fullname', 'DOB','phone','country','update', 'ref_code'];

    public static function user(){
        return $this->belongsTo('App\Models\User');
    }

    public function saveProfile($request, $data){
      $profile = $this->where('user_id', $data->id)->first();
        if(is_null($profile)){
            $saved = $this->create([
                'user_id' => $data->id,
                'fullname' => '',
                'DOB' => '',
                'phone' => '',
                'country' => '',
                'ref_code' => str_random(15),
                'update' => 0,
            ]);
            if($saved){
                $data = [
                    $this->createWallet($request, $data),
                    $this->setWidthrawalAddress($request, $data)
                ];
                if($data){
                    return true;
                }
                dd("error oooh");
            }
        }else{
            $profile->fullname = $request->fullname;
            $profile->Phone = $request->phone;
            $profile->DOB = $request->dob;
            $profile->country = $request->country;
            $profile->update = 1;
            if($profile->save()){
                return back()->with('success', 'profile Succefully Updated');
            }
        }
    }
    
    public function createWallet($request, $data){
        $profile = Wallet::where('user_id', $data->id)->first();
          if(is_null($profile)){
              $saved = Wallet::create([
                  'user_id' => $data->id,
                  'amount' => encrypt(0),
                  'ref_amount' => encrypt(0)
              ]);
              if($saved){
                return true;
              }
        }
    }
    
    public function setWidthrawalAddress($request, $data){
        $profile = WAddress::where('user_id', $data->id)->first();
        if(is_null($profile)){
            $saved = WAddress::create([
                'user_id' => $data->id,
                'currency' => 'ETH',
                'address' => encrypt(''),
                'update' => 0,
            ]);
            if($saved){
            return true;
            }
        }else{
            $profile->address = encrypt($request->swallet_address);
            $profile->currency = "ETH";
            $profile->update = 1;
            if($profile->save()){
                return back()->with('success', 'Withdrawal Address Successfully Updated');
            }
        }
    }
    
    public function WalletTotal(){
        return $this->hasOne('App\Models\Wallet', 'user_id');
    }
}
