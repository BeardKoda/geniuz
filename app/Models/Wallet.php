<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Wallet extends Model
{
    //
    protected $fillable = ['user_id', 'amount', 'ref_amount', 'status'];

    public function user(){
        return $this->belongsTo('App\Models\User');
    }
}
