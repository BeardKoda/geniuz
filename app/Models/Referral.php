<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Referral extends Model
{
    protected $fillable = [
        'user_id', 'refer_id', 'ref_code', 'status'
    ];

    public function validCode($code){
        $user = \App\Models\profile::where('ref_code', $code)->first();
        if(is_null($user)){
            return false;
        }
        return true;
    }

    public function saveRefers($refCode, $data){
        $user = \App\Models\profile::where('ref_code', $refCode)->first();
        $saved = $this->create([
            'user_id' => $data->id,
            'refer_id' => $user->id,
            'ref_code' => $refCode,
            'status' => encrypt(false)
        ]);
        if($saved){
            return true;
        }
    }

    public function updateRefers($refCode, $id){
        $uss = \App\Models\Transaction::where('user_id', $id)->first();
        if($uss){
            $user = \App\Models\profile::where('ref_code', $refCode)->first();
            if($this->saveUserBonus($user->user_id)){
                $saved = $this->create([
                    'user_id' => $id,
                    'refer_id' => $user->id,
                    'ref_code' => $refCode,
                    'paid' => encrpt(false)
                ]);

                if($saved){
                    return true;
                }
            }
            
        }
    }

    public function saveUserBonus($id){
        $wallet = \App\Models\Wallet::where('user_id', $id)->first();
        $oldamount = decrypt($wallet->amount);
        $bonus = 30;
        $amount = $bonus + $oldamount;
        $wallet->amount = encrypt($amount);
        $wallet->ref_amount = encrypt($bonus);
        if($wallet->save()){
            return true;
        }
    } 
}
