function ReferralCheck() {
    // Get the checkbox
    var checkBox = document.getElementById("referCheck");
    // Get the output text
    var text = document.getElementById("referBox");

    // If the checkbox is checked, display the output text
    if (checkBox.checked == true) {
        text.style.display = "block";
    } else {
        text.style.display = "none";
    }
}

function ActivateButton() {
    var checkBox = document.getElementById("term-condition");
    var siginup = document.getElementById("signup");
    if (checkBox.checked == true) {
        siginup.disabled = false;
    } else {
        siginup.disabled = true;
    }
}