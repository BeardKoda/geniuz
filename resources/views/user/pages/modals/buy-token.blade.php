    <div class="modal fade" id="get-pay-address" tabindex="-1">
        <div class="modal-dialog modal-dialog-md modal-dialog-centered">
            <div class="modal-content"><a href="#" class="modal-close" data-dismiss="modal" aria-label="Close"><em class="ti ti-close"></em></a>
                <div class="popup-body">
                    <h4 class="popup-title">Payment Address for Deposit</h4>
                    <p>Please make deposit amount of <strong>1.0 ETH</strong> to our address and receive <strong>18,750 TWZ</strong> tokens including <strong>bonus 1,540 TWZ</strong> once we recevied payment.</p>
                    <div class="gaps-1x"></div>
                    <h6 class="font-bold">Payment to the following Address</h6>
                    <div class="copy-wrap mgb-0-5x"><span class="copy-feedback"></span><em class="fab fa-ethereum"></em><input type="text" class="copy-address" value="0x4156d3342d5c385a87d264f90653733592000581" disabled=""><button class="copy-trigger copy-clipboard" data-clipboard-text="0x4156d3342d5c385a87d264f90653733592000581"><em class="ti ti-files"></em></button></div>
                    <!-- .copy-wrap 
                    <ul class="pay-info-list row">
                        <li class="col-sm-6"><span>SET GAS LIMIT:</span> 120 000</li>
                        <li class="col-sm-6"><span>SET GAS PRICE:</span> 95 Gwei</li>
                    </ul>
                    <-- .pay-info-list -->
                    <div class="pdb-2-5x pdt-1-5x"><input type="checkbox" class="input-checkbox input-checkbox-md" id="agree-term"><label for="agree-term">I hereby agree to the <strong>token purchase aggrement &amp; token sale term</strong>.</label></div>
                    <button id="submitt" class="btn btn-primary" type="submit">Buy Tokens Now  <em class="ti ti-arrow-right mgl-4-5x"></em>
                    </button>
                    {{-- <button class="btn btn-primary" type="submit"
                        data-dismiss="modal" data-toggle="modal" data-target="#pay-confirm">Buy Tokens Now  <em class="ti ti-arrow-right mgl-4-5x"></em>
                    </button> --}}
                    <div class="gaps-3x"></div>
                    <div class="note note-plane note-light mgb-1x"><em class="fas fa-info-circle"></em>
                        <p>Do not make payment through exchange (Kraken, Bitfinex). You can use MayEtherWallet, MetaMask, Mist wallets etc.</p>
                    </div>
                    <div class="note note-plane note-danger"><em class="fas fa-info-circle"></em>
                        <p>In case you send a different amount, number of TWZ tokens will update accordingly.</p>
                    </div>
                </div>
            </div>
            <!-- .modal-content -->
        </div>
        <!-- .modal-dialog -->
    </div>
    <div class="modal fade" id="loader" tabindex="-1">
        <div class="modal-dialog modal-dialog-md modal-dialog-centered">
            <div class="modal-content">
                <div class="popup-body row">
                    <div class="col-8 mx-auto">
                        <div id="response" class=" text-center">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal End -->
    <div class="modal fade" id="pay-confirm" tabindex="-1">
        <div class="modal-dialog modal-dialog-md modal-dialog-centered">
            <div class="modal-content">
                <div class="popup-body">
                    <h4 class="popup-title">Confirmation Your Payment</h4>
                    <p class="lead text-primary">Your Order no. <strong>TNX93KR8N0</strong> has been placed successfully. </p>
                    <p>The tokens balance will appear in your account only after you transaction gets 6 confirmations and approved our team.</p>
                    <p>To <strong>speed up verifcation</strong> proccesing please enter <strong>your wallet address</strong> from where you’ll transferring your ethereum to our address.</p>
                    <div class="input-item input-with-label"><label for="token-address" class="input-item-label">Enter your wallet address</label><input class="input-bordered" type="text" value="0xde0b295669a9fd93d5f28d9ec85e40f4cb697bae"></div>
                    <!-- .input-item -->
                    <ul class="d-flex flex-wrap align-items-center guttar-30px">
                        <li><a href="#" data-dismiss="modal" data-toggle="modal" data-target="#pay-review" class="btn btn-primary">Confirm Payment</a></li>
                        <li class="pdt-1x pdb-1x"><a href="#" data-dismiss="modal" data-toggle="modal" data-target="#pay-online" class="link link-primary">Make Online Payment</a></li>
                    </ul>
                    <div class="gaps-2x"></div>
                    <div class="gaps-1x d-none d-sm-block"></div>
                    <div class="note note-plane note-light mgb-1x"><em class="fas fa-info-circle"></em>
                        <p>Do not make payment through exchange (Kraken, Bitfinex). You can use MayEtherWallet, MetaMask, Mist wallets etc.</p>
                    </div>
                    <div class="note note-plane note-danger"><em class="fas fa-info-circle"></em>
                        <p>In case you send a different amount, number of TWZ tokens will update accordingly.</p>
                    </div>
                </div>
            </div>
            <!-- .modal-content -->
        </div>
        <!-- .modal-dialog -->
    </div>
    <!-- Modal End -->
    <div class="modal fade" id="pay-online" tabindex="-1">
        <div class="modal-dialog modal-dialog-md modal-dialog-centered">
            <div class="modal-content pb-0">
                <div class="popup-body">
                    <h4 class="popup-title">Buy Tokens and Payment</h4>
                    <p class="lead">To receiving <strong>18,750 TWZ</strong> tokens including <strong>bonus 1,540 TWZ</strong> require payment amount of <strong>1.0 ETH</strong>.</p>
                    <p>You can choose any of following payment method to make your payment. The tokens balance will appear in your account after successfull payment.</p>
                    <h5 class="mgt-1-5x font-mid">Select payment method:</h5>
                    <ul class="pay-list guttar-20px">
                        <li class="pay-item"><input type="radio" class="pay-check" name="pay-option" id="pay-coin"><label class="pay-check-label" for="pay-coin"><img src="images/pay-a.png" alt="pay-logo"></label></li>
                        <li class="pay-item"><input type="radio" class="pay-check" name="pay-option" id="pay-coinpay"><label class="pay-check-label" for="pay-coinpay"><img src="images/pay-b.png" alt="pay-logo"></label></li>
                        <li class="pay-item"><input type="radio" class="pay-check" name="pay-option" id="pay-paypal"><label class="pay-check-label" for="pay-paypal"><img src="images/pay-c.png" alt="pay-logo"></label></li>
                    </ul><span class="text-light font-italic mgb-2x"><small>* Payment gateway company may charge you a processing fees.</small></span>
                    <div class="pdb-2-5x pdt-1-5x"><input type="checkbox" class="input-checkbox input-checkbox-md" id="agree-term-3"><label for="agree-term-3">I hereby agree to the <strong>token purchase aggrement &amp; token sale term</strong>.</label></div>
                    <ul class="d-flex flex-wrap align-items-center guttar-30px">
                        <li><a href="#" data-dismiss="modal" data-toggle="modal" data-target="#pay-review" class="btn btn-primary">Buy Tokens &amp; Process to Pay <em class="ti ti-arrow-right mgl-2x"></em></a></li>
                        <li class="pdt-1x pdb-1x"><a href="#" data-dismiss="modal" data-toggle="modal" data-target="#get-pay-address" class="link link-primary">Make Manual Payment</a></li>
                    </ul>
                    <div class="gaps-2x"></div>
                    <div class="gaps-1x d-none d-sm-block"></div>
                    <div class="note note-plane note-light mgb-1x"><em class="fas fa-info-circle"></em>
                        <p class="text-light">You will automatically redirect for payment after your order placing.</p>
                    </div>
                </div>
            </div>
            <!-- .modal-content -->
        </div>
        <!-- .modal-dialog -->
    </div>
    <!-- Modal End -->
    <div class="modal fade" id="pay-coingate" tabindex="-1">
        <div class="modal-dialog modal-dialog-md modal-dialog-centered">
            <div class="modal-content">
                <div class="popup-body">
                    <h4 class="popup-title">Buy Tokens and Payment</h4>
                    <p class="lead">To receiving <strong>18,750 TWZ</strong> tokens including <strong>bonus 1,540 TWZ</strong> require payment amount of <strong>1.0 ETH</strong>.</p>
                    <p>You can pay via online payment geteway <strong>“Coingate”</strong>. It’s safe and secure.</p>
                    <div class="pdb-2-5x pdt-1-5x"><input type="checkbox" class="input-checkbox input-checkbox-md" id="agree-term-2"><label for="agree-term-2">I hereby agree to the <strong>token purchase aggrement &amp; token sale term</strong>.</label></div><button class="btn btn-primary"
                        data-dismiss="modal" data-toggle="modal" data-target="#pay-thanks">Place Order &amp; Process to Pay  <em class="ti ti-arrow-right mgl-4-5x"></em></button>
                    <div class="gaps-3x"></div>
                    <div class="note note-plane note-light mgb-1x"><em class="fas fa-info-circle"></em>
                        <p>You will automatically redirect to Coingate website for payment</p>
                    </div>
                </div>
            </div>
            <!-- .modal-content -->
        </div>
        <!-- .modal-dialog -->
    </div>
    <!-- Modal End -->
    <div class="modal fade" id="pay-thanks" tabindex="-1">
        <div class="modal-dialog modal-dialog-md modal-dialog-centered">
            <div class="modal-content">
                <div class="popup-body text-center">
                    <div class="gaps-2x"></div>
                    <div class="pay-status pay-status-success"><em class="ti ti-check"></em></div>
                    <div class="gaps-2x"></div>
                    <h3>Thanks for your contribution!</h3>
                    <p>Your payment amount <strong>1.0 ETH</strong> has been successfully received againest order no. <strong>TNX94KR8N0</strong>. We’ve added <strong>18,750 TWZ</strong> tokens in account.</p>
                    <div class="gaps-2x"></div><a href="ico-distribution.html" class="btn btn-primary">See Token Balance</a>
                    <div class="gaps-1x"></div>
                </div>
            </div>
            <!-- .modal-content -->
        </div>
        <!-- .modal-dialog -->
    </div>
    <!-- Modal End -->
    <div class="modal fade" id="pay-review" tabindex="-1">
        <div class="modal-dialog modal-dialog-md modal-dialog-centered">
            <div class="modal-content">
                <div class="popup-body text-center">
                    <div class="gaps-2x"></div>
                    <div class="pay-status pay-status-success"><em class="ti ti-check"></em></div>
                    <div class="gaps-2x"></div>
                    <h3>We’re reviewing your payment.</h3>
                    <p>We’ll review your transaction and get back to your within 6 hours. You’ll receive an email with the details of your contribution.</p>
                    <div class="gaps-2x"></div><a href="transactions.html" class="btn btn-primary">View Transaction</a>
                    <div class="gaps-1x"></div>
                </div>
            </div>
            <!-- .modal-content -->
        </div>
        <!-- .modal-dialog -->
    </div>
    <!-- Modal End -->
    <div class="modal fade" id="pay-failed" tabindex="-1">
        <div class="modal-dialog modal-dialog-md modal-dialog-centered">
            <div class="modal-content">
                <div class="popup-body text-center">
                    <div class="gaps-2x"></div>
                    <div class="pay-status pay-status-error"><em class="ti ti-alert"></em></div>
                    <div class="gaps-2x"></div>
                    <h3>Oops! Payment failed!</h3>
                    <p>Sorry, seems there is an issues occurred and we couldn’t process your payment. If you continue to have issues, please contact us with order no. <strong>TNX94KR8N0</strong>.</p>
                    <div class="gaps-2x"></div><a href="#" data-dismiss="modal" data-toggle="modal" data-target="#get-pay-address" class="btn btn-primary">Try to Pay Again</a>
                    <div class="gaps-1x"></div>
                </div>
            </div>
            <!-- .modal-content -->
        </div>
        <!-- .modal-dialog -->
    </div>
    <!-- Modal End -->