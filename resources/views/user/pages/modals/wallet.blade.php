<div class="modal fade" id="edit-wallet" tabindex="-1">
        <div class="modal-dialog modal-dialog-md modal-dialog-centered">
            <div class="modal-content"><a href="#" class="modal-close" data-dismiss="modal" aria-label="Close"><em
                        class="ti ti-close"></em></a>
                <div class="popup-body">
                    <h4 class="popup-title">Wallet Address</h4>
                    <p>In order to receive your <a href="#"><strong>GMC Tokens</strong></a>, please select your wallet
                        address and you have to put the address below input box. <strong>You will receive TWZ tokens to
                            this address after the Token Sale end.</strong></p>
                    <form action="{{route('profile.wallet')}}" method="POST">
                        @csrf
                        <div class="row">
                            <div class="col-md-6">
                                <div class="input-item input-with-label">
                                    <label for="swallet" class="input-item-label">Select Wallet </label>
                                    <select class="select-bordered select-block" name="swallet" id="swallet" readonly>
                                        <option value="eth">Ethereum</option>
                                    </select>
                                </div>
                                <!-- .input-item -->
                            </div><!-- .col -->
                        </div><!-- .row -->
                        <div class="input-item input-with-label"><label for="token-address"
                                class="input-item-label">Your Address for tokens:</label><input class="input-bordered"
                                type="text" id="token-address" name="swallet_address"
                                value="{{decrypt(Auth::user()->withdrawlAddress->address)}}"><span class="input-note">Note:
                                Address should be ERC20-compliant.</span></div><!-- .input-item -->
                        <div class="note note-plane note-danger"><em class="fas fa-info-circle"></em>
                            <p>DO NOT USE your exchange wallet address such as Kraken, Bitfinex, Bithumb, Binance etc.
                                You can use MetaMask, MayEtherWallet, Mist wallets etc. Do not use the address if you
                                don’t have a private key of the your address. You WILL NOT receive TWZ Tokens and WILL
                                LOSE YOUR FUNDS if you do.</p>
                        </div>
                        <div class="gaps-3x"></div>
                        <div class="d-sm-flex justify-content-between align-items-center"><button
                                class="btn btn-primary">Update Wallet</button>
                            <div class="gaps-2x d-sm-none"></div><span class="text-success"><em
                                    class="ti ti-check-box"></em> Updated wallet address</span>
                        </div>
                    </form><!-- form -->
                </div>
            </div><!-- .modal-content -->
        </div><!-- .modal-dialog -->
    </div><!-- Modal End -->