<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="author" content="Softnio">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <!-- Fav Icon -->
    <link rel="shortcut icon" href="/files/images/geniuzcoin.ico"><!-- Site Title  -->
    <title>{{ config('app.name') }}</title><!-- Vendor Bundle CSS -->
    <link rel="stylesheet" href="/files/assets/css/vendor.bundle49f7.css?ver=104"><!-- Custom styles for this template -->
    <link rel="stylesheet" href="/files/assets/css/style.css" id="layoutstyle">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
</head>

<body class="page-user">
    @include('user.includes.topbar')
    <div class="page-content">
        <div class="container">
            @include('user.flash')
            @yield('content')
        </div><!-- .container -->
    </div><!-- .page-content -->
    @include('user.includes.footer')
            @yield('javascript')
    <!-- JavaScript (include all script here) -->
    <script src="/files/assets/js/jquery.bundle49f7.js?ver=104"></script>
    <script src="/files/assets/js/script49f7.js?ver=104"></script>
    <script src="/files/assets/js/toastr.examples49f7.js?ver=104" defer></script>
</body>

</html>
