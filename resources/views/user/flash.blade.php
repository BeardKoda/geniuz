@if ($message = Session::get('success'))
    <div class="card-innr card-innr-fix">
        <div class="alert alert-success-alt alert-dismissible fade show"> 
              <em class="ti ti-check-box"></em>{{ $message }}
        </div>
    </div>
@endif

@if ($message = Session::get('error'))
    <div class="card-innr card-innr-fix">
  <div class="alert alert-danger-alt alert-dismissible fade show">
    {{ $message }}
  </div>
  </div>
@endif

@if ($message = Session::get('warning'))
    <div class="card-innr card-innr-fix">
  <div class="alert alert-warning-alt alert-dismissible fade show">
    {{ $message }}
  </div>
  </div>
@endif


@if ($message = Session::get('info'))
    <div class="card-innr card-innr-fix">
        <div class="alert alert-secondary-alt alert-dismissible fade show"> 
              <em class="ti ti-check-box"></em>{{ $message }}
        </div>
    </div>
@endif

@if ($errors->any())
    <div class="card-innr card-innr-fix">
  <div class="alert alert-primary-alt alert-dismissible fade show">
    {{ $message }}
  </div>
  </div>
@endif
