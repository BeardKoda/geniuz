
    <div class="topbar-wrap">
        <div class="topbar is-sticky">
            <div class="container">
                <div class="d-flex justify-content-between align-items-center">
                    <ul class="topbar-nav d-lg-none">
                        <li class="topbar-nav-item relative"><a class="toggle-nav" href="#">
                                <div class="toggle-icon"><span class="toggle-line"></span><span
                                        class="toggle-line"></span><span class="toggle-line"></span><span
                                        class="toggle-line"></span></div>
                            </a></li><!-- .topbar-nav-item -->
                    </ul><!-- .topbar-nav --><a class="topbar-logo" href="index-2.html"><img
                            src="/logo.png" srcset="/logo.png" alt="logo"></a>
                    <ul class="topbar-nav">
                        <li class="topbar-nav-item relative">
                            <span class="user-welcome d-none d-lg-inline-block">Welcome! {{Auth::user()->username}}</span><a
                                class="toggle-tigger user-thumb" href="#"><em class="ti ti-user"></em></a>
                            <div
                                class="toggle-class dropdown-content dropdown-content-right dropdown-arrow-right user-dropdown">
                                <div class="user-status">
                                    <h6 class="user-status-title">GMC balance</h6>
                                    <div class="user-status-balance">{{decrypt(Auth::user()->gencWallet->amount)}}  <small>GENC</small></div>
                                </div>
                                <ul class="user-links">
                                    <li><a href="{{route('profile')}}"><i class="ti ti-id-badge"></i>My Profile</a></li>
                                    <li><a href="#"><i class="ti ti-infinite"></i>Referral</a></li>
                                </ul>
                                <ul class="user-links bg-light">
                                    <li><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                        <i class="ti ti-power-off"></i>
                                            {{ __('Logout') }}
                                        </a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    </li>
                                </ul>
                            </div>
                        </li><!-- .topbar-nav-item -->
                    </ul><!-- .topbar-nav -->
                </div>
            </div><!-- .container -->
        </div><!-- .topbar -->
        <div class="navbar">
            <div class="container">
                <div class="navbar-innr">
                    <ul class="navbar-menu">
                        <li class="{{ request()->is('user/dashboard') ? 'active' : '' }}"><a href="{{route('dashboard')}}"><em class="ikon ikon-dashboard"></em> Dashboard</a></li>
                        <li class="{{ request()->is('user/buy-token') ? 'active' : '' }}"><a href="{{route('buy-token')}}"><em class="ikon ikon-coins"></em> Buy Tokens</a></li>
                        <li class="{{ request()->is('user/ICO') ? 'active' : '' }}"><a href="#"><em class="ikon ikon-distribution"></em> ICO
                                Distribution</a></li>
                        <li class="{{ request()->is('user/transactions') ? 'active' : '' }}"><a href="{{route('transactions')}}"><em class="ikon ikon-transactions"></em> Transactions</a></li>
                        <li class="{{ request()->is('user/profile') ? 'active' : '' }}"><a href="{{route('profile')}}"><em class="ikon ikon-user"></em> Profile</a></li>
                    
                    </ul>
                    <ul class="navbar-btns">
                        <!-- <li><a href="#" class="btn btn-sm btn-outline btn-light"><em class="text-primary ti ti-files"></em><span>KYC Application</span></a></li> -->
                        <li class="d-none"><span class="badge badge-outline badge-success badge-lg"><em
                                    class="text-success ti ti-files mgr-1x"></em><span class="text-success">KYC
                                    Approved</span></span></li>
                    </ul>
                </div><!-- .navbar-innr -->
            </div><!-- .container -->
        </div><!-- .navbar -->
    </div><!-- .topbar-wrap -->