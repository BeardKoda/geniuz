<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="author" content="Softnio">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <!-- Fav Icon -->
    <link rel="shortcut icon" href="/files/images/favicon.png"><!-- Site Title  -->
    <title>{{ config('app.name') }}</title><!-- Vendor Bundle CSS -->
    <link rel="stylesheet" href="/files/assets/css/vendor.bundle49f7.css?ver=104"><!-- Custom styles for this template -->
    <link rel="stylesheet" href="/files/assets/css/style.css" id="layoutstyle">
    
</head>

<body class="page-user">
    <div class="page-ath-wrap">
        <div class="page-ath-content">
            <div class="page-ath-header">
                <a href="#" class="page-ath-logo"><img src="/front/images/geniuz-logo.png" srcset="/front/images/geniuz-logo.png" style='max-width: 30vh;' alt="logo"></a>
            </div>
            @if ($errors->any())
                <div class="card-innr card-innr-fix"> 
                        <ul>
                            @foreach ($errors->all() as $error)
                                <div class="alert alert-warning-alt alert-dismissible fade show">
                                    <li><strong>{{ $error }}</strong></li>
                                </div>
                            @endforeach
                        </ul>
                </div>
            @endif 
            <div class="page-ath-form">
                <h2 class="page-ath-heading">Sign in <small>with your Geniuz Account</small></h2>
                <form method="POST" action="{{ route('login') }}">
                    @csrf
                    <div class="input-item">
                        <label for="password-confirm" class="col-form-label text-md-right">{{ __('Email') }}</label>
                        <input type="text" name="email" placeholder="Email" class="input-bordered">                        
                        @error('email')
                            <span class="invalid-feedback"role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="input-item">
                        <label for="password-confirm" class="col-form-label text-md-right">{{ __('Password') }}</label>
                        <input type="password" placeholder="Password" name="password" class="input-bordered">
                        @error('password')
                            <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                        @enderror
                    </div>
                    <div class="d-flex justify-content-between align-items-center">
                        <div class="input-item text-left"><input class="input-checkbox input-checkbox-md" id="remember-me" type="checkbox"><label for="remember-me">Remember Me</label></div>
                        @if (Route::has('password.request'))
                            <div>
                                <a href="{{ route('password.request') }}">Forgot password?</a>
                                <div class="gaps-2x"></div>
                            </div>
                        @endif
                    </div><button class="btn btn-primary btn-block">Sign In</button>
                </form>
                <div class="gaps-2x"></div>
                <div class="gaps-2x"></div>
                <div class="form-note">Don’t have an account?
                    <a href="{{route('register')}}"> <strong>Sign up here</strong></a>
                </div>
            </div>
            <div class="page-ath-footer">
                <ul class="footer-links">
                    <li><a href="regular-page.html">Privacy Policy</a></li>
                    <li><a href="regular-page.html">Terms</a></li>
                    <li>&copy; 2019 Geniuz.</li>
                </ul>
            </div>
        </div>
        <div class="page-ath-gfx">
            <div class="w-100 d-flex justify-content-center">
                <div class="col-md-8 col-xl-5"><img src="/files/images/ath-gfx.png" alt="image"></div>
            </div>
        </div>
    </div>
    <!-- JavaScript (include all script here) -->
    <script src="/files/assets/js/jquery.bundle49f7.js?ver=104"></script>
    <script src="/files/assets/js/script49f7.js?ver=104"></script>
</body>

</html>