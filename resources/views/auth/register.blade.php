<!DOCTYPE html>
<html lang="zxx" class="js">

<head>
    <meta charset="utf-8">
    <meta name="author" content="Softnio">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Fully featured and complete ICO Dashboard template for ICO backend management.">
    <!-- Fav Icon -->
    <link rel="shortcut icon" href="/files/images/favicon.png">
    <!-- Site Title  -->
    <title>{{ config('app.name') }}</title>
    <!-- Vendor Bundle CSS -->
    <link rel="stylesheet" href="/files/assets/css/vendor.bundle49f7.css?ver=104">
    <!-- Custom styles for this template -->
    <link rel="stylesheet" href="/files/assets/css/style49f7.css?ver=104" id="layoutstyle">
    <link rel="stylesheet" href="/files/assets/css/main.css">
</head>

<body class="page-ath">
    <div class="page-ath-wrap">
        <div class="page-ath-content">
            <div class="page-ath-header">
                <a href="#" class="page-ath-logo"><img src="/front/images/geniuz-logo.png" srcset="/front/images/geniuz-logo.png" style='max-width: 30vh;' alt="logo"></a>
            </div>
            @if ($errors->any())
                <div class="card-innr card-innr-fix">
                    <div class="alert alert-warning-alt alert-dismissible fade show"> 
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li><strong>{{ $error }}</strong></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            @endif 
            <div class="page-ath-form">
                <h2 class="page-ath-heading">Sign up <small>Create New Geniuz Account</small></h2>
                <form method="POST" action="{{ route('register') }}">
                    @csrf

                    <div class="input-item">
                        <input type="text" placeholder="Your Name" name="username"  value="{{ old('username') }}" class="input-bordered @error('username') is-invalid @enderror" value="{{ old('name') }}" required autocomplete="name" autofocus>
                        @error('name')
                            <span class="alert alert-warning-alt alert-dismissible fade show" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="input-item">
                        <input type="text" placeholder="Your Email" name="email" value="{{ old('email') }}" class="input-bordered @error('email') is-invalid @enderror" required autocomplete="email">
                        @error('email')
                            <span class="alert alert-primary-alt alert-dismissible fade show" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="input-item">
                        <input type="password" placeholder="Password" name="password" class="input-bordered @error('password') is-invalid @enderror" required autocomplete="new-password">
                        @error('password')
                            <span class="alert alert-primary-alt alert-dismissible fade show" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="input-item">
                        <input type="password" placeholder="Repeat Password" class="input-bordered" name="password_confirmation" required autocomplete="new-password">
                        @error('password')
                            <span class="alert alert-primary-alt alert-dismissible fade show" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    @isset(request()->ref)
                    <div class="input-item text-left">
                        <input class="input-checkbox input-checkbox-md" 
                        id="referCheck" type="checkbox" disabled checked onclick="ReferralCheck()">
                        <label for="referCheck">Enter Referral Code
                        </label>
                        <div id="referBox">
                            <input type="Text" class="input-bordered refer" value="{{request()->ref}}" name="referral" readonly autocomplete="new-password">
                        </div>
                        @error('referral')
                            <span class="alert alert-primary-alt alert-dismissible fade show" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    @else
                    <div class="input-item text-left">
                        <input class="input-checkbox input-checkbox-md" 
                        id="referCheck" type="checkbox" onclick="ReferralCheck()">
                        <label for="referCheck">Enter Referral Code
                        </label>
                        <div id="referBox" style="display:none;">
                            <input type="text" class="input-bordered refer"  name="referral" placeholder="Enter Code">
                        </div>
                        @error('referral')
                            <span class="alert alert-primary-alt alert-dismissible fade show" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    @endisset
                    <div class="input-item text-left">
                        <input class="input-checkbox input-checkbox-md" 
                        id="term-condition" required type="checkbox" onclick="ActivateButton()">
                        <label for="term-condition">I agree to Geniuz’s 
                            <a href="#">Privacy Policy</a> &amp;<a href="#">Terms.</a>
                        </label>
                    </div>
                    <button type="submit" id="signup" class="btn btn-primary btn-block" disabled>
                        Create Account
                    </button>
                </form>
                <div class="gaps-2x"></div>
                <div class="gaps-2x"></div>
                <div class="form-note">Already have an account ?
                    <a href="{{route('login')}}"> <strong>Sign in
                            instead</strong></a>
                </div>
            </div>
            <div class="page-ath-footer">
                <ul class="footer-links">
                    <li><a href="regular-page.html">Privacy Policy</a></li>
                    <li><a href="regular-page.html">Terms</a></li>
                    <li>&copy; 2019 Geniuz.</li>
                </ul>
            </div>
        </div>
        <div class="page-ath-gfx">
            <div class="w-100 d-flex justify-content-center">
                <div class="col-md-8 col-xl-5"><img src="/files/images/ath-gfx.png" alt="image"></div>
            </div>
        </div>
    </div>
    <!-- JavaScript (include all script here) -->
    <script src="/files/assets/js/jquery.bundle49f7.js?ver=104"></script>
    <script src="/files/assets/js/main.js"></script>
    <script src="/files/assets/js/script49f7.js?ver=104"></script>
</body>
<!-- Mirrored from demo.themenio.com/tokenwiz/sign-up.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 20 May 2019 22:35:20 GMT -->

</html>