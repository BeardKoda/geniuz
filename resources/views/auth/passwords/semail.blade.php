<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="author" content="Softnio">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <!-- Fav Icon -->
    <link rel="shortcut icon" href="/files/images/favicon.png"><!-- Site Title  -->
    <title>{{ config('app.name') }}</title><!-- Vendor Bundle CSS -->
    <link rel="stylesheet" href="/files/assets/css/vendor.bundle49f7.css?ver=104"><!-- Custom styles for this template -->
    <link rel="stylesheet" href="/files/assets/css/style.css" id="layoutstyle">
    
</head>
<body class="page-ath">
    <div class="page-ath-wrap">
        <div class="page-ath-content">
            <div class="page-ath-header">
                <a href="index.html" class="page-ath-logo">
                <img src="/front/images/geniuz-logo.png" srcset="/front/images/geniuz-logo.png 2x" 
                style='max-width: 30vh;' alt="logo">
                </a>
            </div>
            <div class="page-ath-form">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif
                <h2 class="page-ath-heading">Reset password 
                    <span>
                        If you forgot your password, well, 
                        then we’ll email you instructions to reset your password.
                    </span>
                </h2>
                <form method="POST" action="{{ route('password.email') }}">
                    @csrf
                    <div class="input-item">
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" 
                        value="{{ old('email') }}" required autocomplete="email" autofocus>
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="d-flex justify-content-between align-items-center">
                        <div>
                            <button type="submit" class="btn btn-primary btn-block">Send Reset Link</button>
                        </div>
                        <div><a href="{{route('login')}}">Return to login</a></div>
                    </div>
                    <div class="gaps-2x"></div>
                </form>
            </div>
            <div class="page-ath-footer">
                <ul class="footer-links">
                    <li><a href="regular-page.html">Privacy Policy</a></li>
                    <li><a href="regular-page.html">Terms</a></li>
                    <li>&copy; 2018 Geniuz.</li>
                </ul>
            </div>
        </div>
        <div class="page-ath-gfx">
            <div class="w-100 d-flex justify-content-center">
                <div class="col-md-8 col-xl-5"><img src="/files/images/ath-gfx.png" alt="image"></div>
            </div>
        </div>
    </div>
    <!-- JavaScript (include all script here) -->
    <script src="/files/assets/js/jquery.bundle49f7.js?ver=104"></script>
    <script src="/files/assets/js/script49f7.js?ver=104"></script>
</body>
</html>